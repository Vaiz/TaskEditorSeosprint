QT       += core gui widgets

TARGET   = UniversalTaskEditor
TEMPLATE = app
CONFIG   += c++11


SOURCES  += \
    src/main.cpp\
    src/mainwindow.cpp

HEADERS  += src/mainwindow.h

FORMS    += src/mainwindow.ui

RESOURCES += \
    src/styles.qrc \
    src/images.qrc

win32: RC_ICONS = src/img/windows.ico
