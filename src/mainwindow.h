#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    
    QString GetQuery();

private slots:
    void on_genereatePushButton_clicked();
    void on_copyPushButton_clicked();
    void on_clearPushButton_clicked();
    void on_openFileButton_clicked();

    QString GetSearchEngine();
    void checkTaskNumber();
    void saveFile();
    void loadFile();
    QStringList parseQuery(QString query);

private:
    bool fileHasSubstring(const QString &string);
    void setFileName(const QString &newFileName);

private:
    Ui::MainWindow *ui;
    QFile m_file;
    QByteArrayList m_fileContent;
    void InitComboBoxs();
    void InitStyleSheet();
};

#endif // MAINWINDOW_H
