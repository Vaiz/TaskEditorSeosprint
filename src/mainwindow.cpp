#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QFile>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include <QApplication>

void MainWindow::InitComboBoxs()
{
    for(int i = 1; i <= 20; ++i)
    {
        ui->siteTransitionComboBox->addItem(QString::number(i));
        ui->googleSiteTransitionComboBox->addItem(QString::number(i));
    }

    for(int i = 5; i <= 65; i += 5)
    {
        ui->delayComboBox->addItem(QString::number(i));
        ui->googleDelayComboBox->addItem(QString::number(i));
    }
}

void MainWindow::InitStyleSheet()
{
    QFile styleF;
    styleF.setFileName(":/styles/styles.qss");
    styleF.open(QFile::ReadOnly);
    QString qssStr = styleF.readAll();
    styleF.close();
    this->setStyleSheet(qssStr);

    ui->fileNameLabel->setProperty("FileChecked", false);
    ui->fileNameLabel->style()->unpolish(ui->fileNameLabel);
    ui->fileNameLabel->style()->polish(ui->fileNameLabel);
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    InitStyleSheet();
    InitComboBoxs();

    connect(ui->taskNumberLineEdit, SIGNAL(editingFinished()),
            this, SLOT(checkTaskNumber()));
}

MainWindow::~MainWindow()
{
    saveFile();
    delete ui;
}

QString MainWindow::GetSearchEngine()
{
    switch(ui->searchEngineComboBox->currentIndex())
    {
    case 0:
        return "google";

    case 1:
        return "yandex";

    default:
        return "unknonwn search engine";
    }
}

QString MainWindow::GetQuery()
{
    QStringList queryList = parseQuery(ui->query1TextEdit->toPlainText())
            + parseQuery(ui->query2TextEdit->toPlainText());

    QString query;
    for(const QString &str : queryList)
        if(!str.isEmpty())
            query += str.simplified() + ",";
    query = query.left(query.length() - 1);

    return query;
}

void MainWindow::on_genereatePushButton_clicked()
{
    if(!ui->fileNameLabel->property("FileChecked").toBool())
    {
        QMessageBox box(QMessageBox::Critical, "Ошибка", "Выберите файл!", QMessageBox::Ok);
        box.exec();
        return;
    }

    QString searchEngine = GetSearchEngine();
    QString query = GetQuery();

    QString task = QString("%1,"
                           "%2,"
                           "%3,"
                           "%4,"
                           "%5,"
                           "%6,"
                           "%7,"
                           "%8,"
                           "%9,"
                           "%10")
            .arg(ui->taskNumberLineEdit->text())                                // 1
            .arg(ui->adSiteLineEdit->text())                                    // 2
            .arg(searchEngine)                                                  // 3
            .arg(ui->siteTransitionComboBox->currentText().toInt())             // 4
            .arg(ui->googleSiteTransitionComboBox->currentText().toInt())       // 5
            .arg(ui->delayComboBox->currentText().toInt())                      // 6
            .arg(ui->googleDelayComboBox->currentText().toInt())                // 7
            .arg(ui->insertIPComboBox->currentIndex())                          // 8
            .arg(ui->requestToReportComboBox->currentIndex())                   // 9
            .arg(query)                                                         // 10
            ;                                                                   // ...

    ui->taskTextEdit->setText(QString("%1%2\r\n")
                              .arg(ui->taskTextEdit->toPlainText())
                              .arg(task));

    m_fileContent.push_front(task.toLocal8Bit());
}

void MainWindow::on_copyPushButton_clicked()
{
    ui->taskTextEdit->selectAll();
    ui->taskTextEdit->copy();
}

void MainWindow::on_clearPushButton_clicked()
{
    ui->taskNumberLineEdit->clear();
    ui->query1TextEdit->clear();
    ui->query2TextEdit->clear();
    ui->adSiteLineEdit->clear();

    checkTaskNumber();
}

void MainWindow::checkTaskNumber()
{
    bool borderRed = false;

    if(!ui->taskNumberLineEdit->text().isEmpty())
        borderRed = fileHasSubstring(ui->taskNumberLineEdit->text());

    if(borderRed)
        ui->taskNumberLineEdit->setProperty("borderRed", true);
    else
        ui->taskNumberLineEdit->setProperty("borderRed", false);

    ui->taskNumberLineEdit->style()->unpolish(ui->taskNumberLineEdit);
    ui->taskNumberLineEdit->style()->polish(ui->taskNumberLineEdit);
}

void MainWindow::saveFile()
{
    if(m_file.fileName().isEmpty())
        return;

    m_file.open(QFile::WriteOnly);
    for(const QByteArray &line : m_fileContent)
    {
        m_file.write(line);
        if(!line.endsWith("\r\n"))
            m_file.write("\r\n");
    }
    m_file.close();
}

void MainWindow::loadFile()
{
    if(m_file.fileName().isEmpty())
        return;

    m_file.open(QFile::ReadOnly);
    while(!m_file.atEnd())
        m_fileContent += m_file.readLine();
    m_file.close();
}

QStringList MainWindow::parseQuery(QString query)
{
    query.replace(",", " ");
    return query.split("\n");
}

bool MainWindow::fileHasSubstring(const QString &string)
{
    QByteArray searchedString(string.toLocal8Bit());

    for(const QByteArray &line : m_fileContent)
        if(line.startsWith(searchedString))
            return true;

    return false;
}

void MainWindow::setFileName(const QString &newFileName)
{
    QStringList path = newFileName.split("/");

    if(path.size() > 2)
    {
        path.pop_back();
        path.pop_back();
        ui->fileNameLabel->setText(path.back());
    }
    else
        ui->fileNameLabel->setText(newFileName);

    ui->fileNameLabel->setProperty("FileChecked", true);
    ui->fileNameLabel->style()->unpolish(ui->fileNameLabel);
    ui->fileNameLabel->style()->polish(ui->fileNameLabel);
}

void MainWindow::on_openFileButton_clicked()
{
    QFileDialog fileDialog;

    if(qApp->arguments().size() > 1)
        fileDialog.setDirectory(qApp->arguments().at(1));
    else
        fileDialog.setDirectory("C:/CaNum");

    if(!fileDialog.exec())
        return;

    saveFile();

    QString newFileName = fileDialog.selectedFiles().first();
    m_file.setFileName(newFileName);
    loadFile();

    ui->taskTextEdit->clear();

    setFileName(newFileName);
}
